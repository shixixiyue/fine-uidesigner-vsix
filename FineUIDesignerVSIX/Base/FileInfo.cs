﻿using Newtonsoft.Json;
using System.IO;

namespace FineUICoreDesigner.Base
{
    /// <summary>
    ///当前文件信息
    /// </summary>
    public class EditFileInfo
    {
        /// <summary>
        /// 当前文件信息
        /// </summary>
        public EditFileInfo(string _FilePath)
        {
            if (!string.IsNullOrEmpty(_FilePath))
            {
                FilePath = _FilePath;
                FileName = new DirectoryInfo(FilePath).Name;
                FileRootPath = Path.GetDirectoryName(FilePath);
                FileContent = File.ReadAllText(FilePath);
                DesignerFileName = FileName + ".designer.cs";
                DesignerFilePath = FileRootPath + "\\" + DesignerFileName;
            }
        }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; private set; }

        /// <summary>
        /// 文件路径
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// 文件根路径
        /// </summary>
        public string FileRootPath { get; private set; }

        /// <summary>
        /// .cs文件路径
        /// </summary>
        public string CsFileName { get; set; }

        /// <summary>
        /// Designer文件名
        /// </summary>
        public string DesignerFileName { get; set; }

        /// <summary>
        /// Designer文件路径
        /// </summary>
        public string DesignerFilePath { get; set; }

        /// <summary>
        /// 文件内容
        /// </summary>
        [JsonIgnore]
        public string FileContent { get; set; }

        /// <summary>
        /// 类名
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 当前的cs类是否是partial
        /// </summary>
        public bool IsPartial { get; set; }

        /// <summary>
        /// 命名空间
        /// </summary>
        public string NamespaceName { get; set; }
    }
}
