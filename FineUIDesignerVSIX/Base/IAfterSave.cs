﻿using FineUICoreDesigner.Base;
using FineUIDesignerVSIX.Handle;

namespace FineUICoreDesigner
{
    /// <summary>
    /// 文档保存后触发
    /// </summary>
    public interface IWeformAfterSave
    {
        /// <summary>
        /// 执行
        /// </summary>
        /// <param name="editFileInfo"></param>
        void Handle(WebformFileInfo editFileInfo);
    }

    /// <summary>
    /// 文档保存后触发
    /// </summary>
    public interface IRazorPageAfterSave
    {
        /// <summary>
        /// 执行
        /// </summary>
        /// <param name="editFileInfo"></param>
        void Handle(RazorPageFileInfo editFileInfo);
    }

    /// <summary>
    /// 文档保存后触发
    /// </summary>
    public interface ICoreMVCAfterSave
    {
        /// <summary>
        /// 执行
        /// </summary>
        /// <param name="editFileInfo"></param>
        void Handle(CoreMVCFileInfo editFileInfo);
    }
}
