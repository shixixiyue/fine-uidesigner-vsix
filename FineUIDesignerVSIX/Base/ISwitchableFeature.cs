﻿using System.Threading.Tasks;

namespace FineUICoreDesigner
{
    internal interface ISwitchableFeature
    {
        Task SwitchAsync(bool on);
    }
}