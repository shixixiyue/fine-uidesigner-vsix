﻿using Microsoft.VisualStudio.VCProjectEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FineUICoreDesigner.Base
{
    public class DesignerConfig
    {
        /// <summary>
        /// 日志等级
        /// </summary>
        //public string LogLevel { get; set; } = "1";

        /// <summary>
        /// 当前页面的项目路径
        /// </summary>
        public string ProjectPath { get; set; }

        /// <summary>
        /// 当前解决方案的路径
        /// </summary>
        public string SolutionPath { set; get; }

        private bool _isWebForms;

        /// <summary>
        /// 是否是 FineUI WebForms 模式，
        /// </summary>
        public bool isWebForms
        {
            get
            {
                if (!isRazorPage) return false;
                return _isWebForms;
            }
            set { _isWebForms = value; }
        }

        /// <summary>
        /// 是否是 FineUI RazorPage 模式，
        /// </summary>
        public bool isRazorPage { get; set; } = false;

        /// <summary>
        /// 是否开启自动生成 Designer
        /// </summary>
        public bool FineUIDesigner { get; set; } = true;
    }
}
