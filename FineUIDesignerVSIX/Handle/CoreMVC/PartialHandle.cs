﻿using FineUICoreDesigner;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FineUIDesignerVSIX.Handle
{
    public class CoreMVCPartialHandle : ICoreMVCAfterSave
    {
        public void Handle(CoreMVCFileInfo editFileInfo)
        {
            if (!editFileInfo.IsPartial)
            {
                var code = BaseHelp.GetCode(editFileInfo.ControllerFilePath);
                if (string.IsNullOrEmpty(code)) return;
                SyntaxTree tree = CSharpSyntaxTree.ParseText(code);
                CompilationUnitSyntax root = tree.GetCompilationUnitRoot();
                var firstClass = root.DescendantNodes()
                                .OfType<ClassDeclarationSyntax>()
                                .FirstOrDefault();
                if (firstClass != null && !firstClass.Modifiers.Any(m => m.IsKind(SyntaxKind.PartialKeyword)))
                {
                    // 如果这个类不是 Partial 类型，就将其改为 Partial 类型
                    ClassDeclarationSyntax newFirstClass = firstClass.AddModifiers(SyntaxFactory.Token(SyntaxKind.PartialKeyword));
                    root = root.ReplaceNode(firstClass, newFirstClass);
                    string newCode = root.NormalizeWhitespace().ToFullString();
                    BaseHelp.WriteAllText(editFileInfo.ControllerFilePath, newCode);
                    BaseHelp.OutputLine($"修改 {editFileInfo.CsFileName} 成功 🎉");
                }
            }
        }
    }
}
