﻿using FineUICoreDesigner;
using FineUICoreDesigner.Base;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.ComponentModel;

namespace FineUIDesignerVSIX.Handle
{
    public class RazorPageFunctionHandle : IRazorPageAfterSave
    {
        /// <summary>
        /// 模板
        /// </summary>
        private const string FunctionTemplate = @"
            public async Task<IActionResult> $EventName$()
            {
                return UIHelper.Result();
            }
            ";

        private string pattern = @"<f:[\w\s=""@.]*?\sOnClick=""(?:(?<handler>@Url\.Handler\(""(?<functionName>[^""]+)""\))|(?<functionName>[^""]+))";

        public void Handle(RazorPageFileInfo editFileInfo)
        {
            var cspath = editFileInfo.FileRootPath + "\\" + editFileInfo.CsFileName;
            var code = BaseHelp.GetCode(cspath);
            if (string.IsNullOrEmpty(code)) return;
            SyntaxTree tree = CSharpSyntaxTree.ParseText(code);
            CompilationUnitSyntax root = tree.GetCompilationUnitRoot();
            var firstClass = root.DescendantNodes()
                            .OfType<ClassDeclarationSyntax>()
                            .FirstOrDefault();
            if (firstClass != null)
            {
                var methods = FindEvents(editFileInfo.FileContent);
                root = AddMethods(editFileInfo, root, firstClass, methods);
            }
        }

        /// <summary>
        /// 新增方法
        /// </summary>
        /// <param name="editFileInfo"></param>
        /// <param name="cspath"></param>
        /// <param name="root"></param>
        /// <param name="firstClass"></param>
        /// <param name="newMethods"></param>
        /// <param name="methods"></param>
        /// <returns></returns>
        private static CompilationUnitSyntax AddMethods(RazorPageFileInfo editFileInfo, CompilationUnitSyntax root, ClassDeclarationSyntax firstClass, List<string> methods)
        {
            var newMethods = new List<MemberDeclarationSyntax>();
            var cspath = editFileInfo.FileRootPath + "\\" + editFileInfo.CsFileName;
            foreach (var item in methods)
            {
                var itemname = "OnPost" + char.ToUpper(item[0]) + item.Substring(1);
                // 检查是否已存在目标方法
                bool methodExists = false;
                foreach (var member in firstClass.Members)
                {
                    if (member is MethodDeclarationSyntax method && method.Identifier.Text == itemname)
                    {
                        methodExists = true;
                        break;
                    }
                }
                if (!methodExists)
                {
                    var methodCode = FunctionTemplate.Replace("$EventName$", itemname);
                    BaseHelp.OutputLine($"生成方法 {itemname} ");
                    MethodDeclarationSyntax newMethod = SyntaxFactory.ParseMemberDeclaration(methodCode) as MethodDeclarationSyntax;
                    newMethods.Add(newMethod.NormalizeWhitespace());
                }
            }
            if (newMethods.Any())
            {
                root = root.ReplaceNode(firstClass, firstClass.AddMembers(newMethods.ToArray()));
                // 重新生成代码
                string newCode = root.NormalizeWhitespace().ToFullString();
                BaseHelp.WriteAllText(cspath, newCode);
                BaseHelp.OutputLine($"添加事件 {editFileInfo.CsFileName} 成功 👏");
            }

            return root;
        }

        private List<string> FindEvents(string fileContent)
        {
            List<string> events = new List<string>();
            MatchCollection matches = Regex.Matches(fileContent, pattern);
            foreach (Match match in matches)
            {
                string functionName = match.Groups["functionName"].Value;
                string handler = match.Groups["handler"].Value;

                events.Add(functionName);
            }
            return events;
        }
    }
}
