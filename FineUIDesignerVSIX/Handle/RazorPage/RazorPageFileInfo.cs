﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FineUICoreDesigner.Base
{
    /// <summary>
    /// RazorPage
    /// </summary>
    public class RazorPageFileInfo : WebformFileInfo
    {
        public RazorPageFileInfo(string _FilePath) : base(_FilePath)
        {
        }
    }
}
