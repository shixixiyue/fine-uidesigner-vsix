﻿using FineUICoreDesigner.Base;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FineUICoreDesigner.Base
{ /// <summary>
  ///当前文件信息
  /// </summary>
    public class WebformFileInfo : EditFileInfo
    {
        /// <summary>
        /// 当前文件信息
        /// </summary>
        public WebformFileInfo(string _FilePath) : base(_FilePath)
        {
            if (!string.IsNullOrEmpty(_FilePath))
            {
                GetCSharpTree();
            }
        }

        /// <summary>
        /// 解析cs文件
        /// </summary>
        private void GetCSharpTree()
        {
            CsFileName = FileName + ".cs";
            var code = File.ReadAllText(FileRootPath + "\\" + CsFileName);
            if (string.IsNullOrEmpty(code)) return;
            SyntaxTree tree = CSharpSyntaxTree.ParseText(code);
            CompilationUnitSyntax root = tree.GetCompilationUnitRoot();
            var firstClass = root.DescendantNodes()
                            .OfType<ClassDeclarationSyntax>()
                            .FirstOrDefault();
            if (firstClass != null)
            {
                ClassName = firstClass.Identifier.Text;
                IsPartial = firstClass.Modifiers.Any(SyntaxKind.PartialKeyword);
                NamespaceName = firstClass?.Ancestors()?
                    .OfType<NamespaceDeclarationSyntax>()?
                    .FirstOrDefault()?.Name?.ToString();
            }
        }
    }
}
