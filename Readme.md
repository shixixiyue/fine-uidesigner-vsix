# 1. 简介

针对开发 `FineUI` 的 `vs` 扩展插件，

功能|适配
---|---
自动生成designer.cs|`FineUICoreMVC` `FineUICoreRazorPage` `FineUICoreWeform`
自动生成事件|`FineUICoreRazorPage` `FineUICoreWeform`
转到定义|`FineUICoreWeform`
转到控制器|`FineUICoreMVC`


> 我本地的是`vs2022`，其他vs版本没有测试；



![](https://blog.shizhuoran.top/static/img/edbb4272995522af82a7d7dd9789f6ff.chajianshengc.gif)

![](https://blog.shizhuoran.top/static/img/decdac9859778f09683a9cf8846956db.è½¬å°å®ä¹.gif)

![](https://blog.shizhuoran.top/static/img/26eb74abd0fd53ed4ac0f1e015de966b.è½¬å°æ§å¶å¨.gif)

[开发日志](https://blog.shizhuoran.top/post/71)

# 2. 使用

直接安装`FineUIDesignerVSIX.vsix`即可使用；[Releases](https://gitee.com/shixixiyue/fine-uidesigner-vsix/releases)

插件会读取 当前项目的 `appsettings.json` 找到 `FineUI` 节点的配置

* 添加 FineUIDesigner:false，以防止生成`Designer`文件
* 添加 FilterType:[] 用于过滤掉不想生成`Designer`的控件类型

``` 示例
{
  "FineUI": {
    "DebugMode": false,
    FineUIDesigner:false,
    "FilterType": [ "Step" ]
  }
}
```

# 3. 原理

通过监听 `.cshtml` 的文件`保存事件`，解析该文本，通过正则匹配到信息生成文件；

核心技术栈|说明
---|---
AdviseRunningDocTableEvents|监听文件事件
IVsRunningDocTableEvents3|监听文件接口
OnAfterSave|文件保存后触发
CSharpSyntaxTree.ParseText|解析代码文本





# 4. 本地开发

目录结构


```
FineUIDesignerVSIX
|---📁Base                              
|------📄BaseHelp.cs                | 公共类，输出控制台方法，读取和重写文件方法
|------📄DesignerConfig.cs          | 配置文件Model（未启用）
|------📄FileInfo.cs                | 当前编辑文件的 Model
|------📄IAfterSave.cs              | 处理类的接口，接收 FileInfo
|---📁Handle
|------📄DesignerHandle.cs          | 生成designer.cs
|------📄FunctionHandle.cs          | 生成.cs事件
|------📄PartialHandle.cs           | .cs文件改为 Partial 
|---📄FineUIDesignerVSIXPackage.cs  | 文件保存事件入口
```

新建类可以继承 `IAfterSave` 接口，实现 `Handle` 方法，接收一个 `EditFileInfo`实例，在 `Handle` 方法中处理；这里我只是写了对于FineUI开发的处理事件，其实可以作为一个框架写自己的逻辑

`EditFileInfo` 包括 文件信息：
字段|类型|说明
---|---|---
FileName|string|文件名称
FilePath|string|文件路径
FileRootPath|string|文件根路径
CsFileName|string|.cs文件路径
DesignerFileName|string|Designer文件名
DesignerFilePath|string|Designer文件路径
FileContent|string|.cshtml文件内容
ClassName|string|当前的cs类名
NamespaceName|string|当前的cs命名空间
IsPartial|bool|当前的cs类是否是partial

生成后安装文件在 `bin\Debug\FineUIDesignerVSIX.vsix`

# 5. 后续

- [ ] 目前事件只匹配了 `onclick`

# 6. 结善缘


![](https://blog.shizhuoran.top/static/img/18c9e1719a9419ba2b2abb07f5e286ae.weixin20.webp)